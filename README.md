# Hardened VLC media player

This repository contains a prototype of a sandbox system integration the VLC
media player.  
A full walkthrough of the design choices of the prototype and a further
description of the sandboxing mechanisms can be found in our global user
[documentation](https://gitlab.com/videolabs/public/sandbox/documentation/-/wikis/home)
on the matter.

For now the **local input** part of the pipeline is sandboxed. Demuxing and ressource
access are isolated in separate low-privilege processes and communicate witht
VLC's broker via IPC.

## License

**VLC** is released under the GPLv2 *(or later)* license and so is this
prototype.

## Platforms

VLC hardened is available for the following platforms:
- [Windows] *(from 10 and later)*
- [GNU/Linux]

An effort to extend the sandbox system support to at least MacOS and BSD is
planned.

## Branches

The default branch of the repository is the latest implementation of the
prototype. We decided to keep track of all the iterations of the sandbox in
separate branches. This allow us to keep a complete history of the versions
while allowing for fixups and keeping track of the commits authorship until the
final branch. It also allow us to rebase on VLC master while keeping a
regression-free version of the sandbox.

These branches follow the `sandbox/N` naming convention starting from
`sandbox/2` for historical reasons, the first iteration (`sandbox/1`) is
currently named `master`.

### Branches description

#### sandbox/4 and sandbox/4.demo

Contains cleanups of the broker code and simplification of the process spawning.
This is also the first version to be tried on windows for a demo. sandbox/4.demo
is a POC version of the sandbox tweaked to build on windows. This branch was
rebased on latest VLC upstream allow vlc_spawn windows implementation and profit
from some slick updates of the QT UI for the demo. This iteration served as a
clean basis for the POC libiris integration.

#### sandbox/5

Stabilization branch, it contains a lot of memory leaks fixes, the broker
together with the services don't leak anymore in a normal use case.

#### sandbox/6

Rebase on the latest version of VLC. ProtoRPC was updated to the latest version.
The main goal of this spin is to finally merge the windows/libiris and interrupt
integrations on top of a healthy branch.

#### sandbox/7

Libiris integration, the access and the demuxer are in sandboxed services.  
Update to the latest protorpc also brings Windows support!

## Build

### Windows

As for VLC, the Windows executables are cross-compiled. It's highly advised to
use one of Videolan's containers image, preferably one using `mingw-llvm` as the
threading backend implementation for C++ defaults does not rely on posix
threads.  
If you are in doubt, go for `registry.videolan.org/vlc-debian-llvm-msvcrt:20221214101739`.

For the building instruction, follow the Windows specific
[guide](<https://gitlab.com/videolabs/public/sandbox/vlc/-/blob/sandbox/6/doc/BUILD-win32.md?ref_type=heads#building-vlc>)
of the official repository. The `build.sh` script has been customized to build
and activate the sandbox automatically.

We highly recommend building with the prebuilt contribs to minimize build time.
You can simply export the following variable before running the build script and
the prebuilt libraries will be fetched automatically.

```bash-session
export VLC_PREBUILT_CONTRIBS_URL=https://artifacts.videolan.org/vlc/win64-llvm/vlc-contrib-x86_64-w64-mingw32-244a884cd0346c0d55a0e58431aa23efd8e373ab.tar.bz2
```

### Linux

You need to manually build the contribs first:
```
cd contrib/
mkdir contrib-linux
cd contrib-linux
../bootstrap --enable-protorpc --enable-libiris
make
```

**Please ensure you have `libcap` headers file installed.**

Run the usual libtool compilation process with the correct flags for sandbox
activation:

```
./bootstrap
mkdir build-linux
../configure --with-contrib=**REPO_ABSOLUTE_PATH**/contrib/**NATIVE_FOLDER** --disable-chromecast --disable-medialibrary --enable-protorpc --enable-rust
make -jN
```

## Run

Running the prototype require disabling some unstable components of vlc via the
command line:

```bash-session
build-linux/vlc **MEDIA_PATH** --no-media-library --no-auto-preparse --rpc
```
