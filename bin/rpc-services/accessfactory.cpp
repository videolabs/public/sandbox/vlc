#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_stream.h>

#include <stdexcept>
#include <cstdio>
#include "protoipc/port.hh"
#include "protoipc/shm.hh"
#include "protorpc/channel.hh"
#include "accessfactory.hh"
#include "interruptible.hh"
#include "access.hh"

#include "capi.h"

#include <iris_worker/iris_worker.h>

void start_factory(const char *access_channel_handle,
                   int access_port_val,
                   const char *interrupt_channel_handle,
                   int interrupt_port_val,
		   const char *shm_pool_str)
{
    ipc::shm::inherit_pool(shm_pool_str);
    // interrupt channel creation
    auto interrupt_port = ipc::Port::from_handle(ipc::Handle::from_desc(interrupt_channel_handle));
    rpc::PortId interrupt_port_id = interrupt_port_val;

    std::printf("[ACCESSFACTORY] Starting out interrupt proxy [interrupt_port_handle=%s, "
                "interrupt_port_id=%lu]\n",
                interrupt_channel_handle, interrupt_port_id);
    rpc::Channel interrupt_channel(interrupt_port_id, std::move(interrupt_port));
    std::thread interrupt_channel_thread([&]() { interrupt_channel.loop(); });

    // access channel creation
    auto access_port = ipc::Port::from_handle(ipc::Handle::from_desc(access_channel_handle));
    rpc::PortId access_port_id = access_port_val;

    std::printf("[ACCESSFACTORY] Starting out of process access factory [access_port_fd=%s, "
                "access_port_id=%lu]\n",
                access_channel_handle, access_port_id);

    rpc::Channel access_channel(access_port_id, std::move(access_port));
    access_channel.bind_static<AccessFactory>(0, &access_channel, &interrupt_channel);

    std::printf("[ACCESSFACTORY] Factory registered, waiting for creation requests ...\n");

    // End of trusted code. Lower privilege to final.
    iris_lower_final_sandbox_privileges_asap();
    access_channel.loop();
    std::printf("[ACCESSFACTORY] access_channel closed\n");

    interrupt_channel_thread.join();
    std::printf("[ACCESSFACTORY] interrupt_channel closed\n");
}


AccessFactory::AccessFactory(rpc::Channel* access_chan, rpc::Channel* interrupt_chan)
    : access_chan_(access_chan), interrupt_chan_(interrupt_chan)
{
    const char* args[] = {
        "-v",
        "--ignore-config",
        "-I",
        "dummy",
        "--no-media-library",
        "--vout=none",
        "--aout=none",
        "--no-rpc",
    };

    vlc_instance_ = capi_libvlc_new(sizeof(args) / sizeof(args[0]), args);

    if (!vlc_instance_)
        throw std::runtime_error("[ACCESSFACTORY] Could not create vlc instance");
}

bool AccessFactory::interrupt_create(std::uint64_t* it_object)
{
    vlc_interrupt_t* interrupt = vlc_interrupt_create();
    *it_object = interrupt_chan_->bind<Interruptible>(interrupt);
    interrupts_[*it_object] = interrupt;
    return true;
}

bool AccessFactory::access_create(std::string url,
                                  bool preparsing,
                                  std::uint64_t interrupt_objectid,
                                  std::uint64_t *stream_object)
{
    std::printf("[ACCESSFACTORY] Creating access for url: %s\n", url.c_str());

    vlc_interrupt_t* it = interrupts_[interrupt_objectid];
    vlc_interrupt_set(it);

    stream_t* stream = capi_vlc_stream_NewURLEx(vlc_instance_, url.c_str(), preparsing);
    if(stream == NULL)
    {
        *stream_object = 0;
        return false;
    }
    *stream_object = access_chan_->bind<Access>(stream, it);

    std::printf("[ACCESSFACTORY] Created access id: %lu\n", *stream_object);

    return true;
}

bool AccessFactory::stop()
{
    access_chan_->stop();
    interrupt_chan_->stop();
    return true;
}

AccessFactory::~AccessFactory()
{
    capi_libvlc_release(vlc_instance_);
}
