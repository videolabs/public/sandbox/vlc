#ifndef RPC_VLC_ACCESSFACTORY_HH
#define RPC_VLC_ACCESSFACTORY_HH

#include <vlc_interrupt.h>

#ifdef __cplusplus
extern "C" {
#endif
void start_factory(const char *access_channel_handle,
                   int access_port_id,
                   const char *interrupt_channel_handle,
                   int interrupt_port_id,
		   const char *shm_pool_str);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#include "streamfactory.sidl.hh"

#include <map>

struct libvlc_instance_t;

class AccessFactory: public vlc::StreamFactoryReceiver
{
public:
    AccessFactory(rpc::Channel* access_chan, rpc::Channel* interrupt_chan);
    ~AccessFactory();
    bool interrupt_create(std::uint64_t* it_object) override;
    bool access_create(std::string url,
                       bool preparsing,
                       std::uint64_t interrupt_objectid,
                       std::uint64_t *stream_object) override;
    bool stop() override;

private:
    rpc::Channel* access_chan_;
    rpc::Channel* interrupt_chan_;
    libvlc_instance_t* vlc_instance_;
    std::map<rpc::ObjectId, vlc_interrupt_t*> interrupts_;
};

#endif
#endif
