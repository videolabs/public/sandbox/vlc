#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_demux.h>
#include <vlc_stream.h>

#include <stdexcept>
#include <cstdio>
#include <thread>
#include <string.h>
#include "protoipc/port.hh"
#include "protoipc/shm.hh"
#include "protorpc/channel.hh"
#include "demuxfactory.hh"
#include "interruptible.hh"
#include "demux.hh"

// Big hax
#include "../../src/rpc/proxify.hh"
#include "rpc/stream.sidl.hh"

#include "capi.h"

#include <iris_worker/iris_worker.h>

void start_factory(const char *demux_channel_handle,
                   int demux_port_val,
                   const char *interrupt_channel_handle,
                   int interrupt_port_val,
		   const char *shm_pool_str)
{
    ipc::shm::inherit_pool(shm_pool_str);
    // interrupt channel creation
    auto interrupt_port = ipc::Port::from_handle(ipc::Handle::from_desc(interrupt_channel_handle));
    rpc::PortId interrupt_port_id = interrupt_port_val;

    std::printf("[DEMUXFACTORY] Starting out interrupt proxy [interrupt_port_fd=%s, "
                "interrupt_port_id=%lu]\n",
                interrupt_channel_handle, interrupt_port_id);
    rpc::Channel interrupt_channel(interrupt_port_id, std::move(interrupt_port));
    std::thread interrupt_channel_thread([&]() { interrupt_channel.loop(); });

    // demux channel creation
    auto demux_port = ipc::Port::from_handle(ipc::Handle::from_desc(demux_channel_handle));
    rpc::PortId demux_port_id = demux_port_val;

    std::printf(
        "[DEMUXFACTORY] Starting out process demux factory [demux_port_fd=%s, demux_port_id=%lu]\n",
        demux_channel_handle, demux_port_id);

    rpc::Channel demux_channel(demux_port_id, std::move(demux_port));
    demux_channel.bind_static<DemuxFactory>(0, &demux_channel, &interrupt_channel);

    std::printf("[DEMUXFACTORY] Factory registered, waiting for creation requests ...\n");

    // End of trusted code. Lower privilege to final.
    iris_lower_final_sandbox_privileges_asap();

    demux_channel.loop();
    std::printf("[DEMUXFACTORY] demux_channel closed\n");

    interrupt_channel_thread.join();
    std::printf("[DEMUXFACTORY] interrupt_channel closed\n");
}

DemuxFactory::DemuxFactory(rpc::Channel* demux_chan, rpc::Channel* interrupt_chan)
    : demux_chan_(demux_chan), interrupt_chan_(interrupt_chan)
{
    const char* args[] = {
        "-vvvv",
        "--ignore-config",
        "-I",
        "dummy",
        "--no-media-library",
        "--vout=none",
        "--aout=none",
        "--no-rpc",
    };

    vlc_instance_ = capi_libvlc_new(sizeof(args) / sizeof(args[0]), args);

    if (!vlc_instance_)
        throw std::runtime_error("[DEMUXFACTORY] Could not create vlc instance");
};

bool DemuxFactory::interrupt_create(std::uint64_t* it_object)
{
    vlc_interrupt_t* interrupt = vlc_interrupt_create();
    *it_object = interrupt_chan_->bind<Interruptible>(interrupt);
    interrupts_[*it_object] = interrupt;
    return true;
}

bool DemuxFactory::demux_create(vlc::RemoteAccess access,
                                vlc::RemoteEsOut out,
                                std::string module,
                                std::string filepath,
                                bool preparsing,
                                std::uint64_t interrupt_objectid,
                                std::uint64_t *demux_object)
{
    vlc_object_t* instance_obj = capi_libvlc_instance_obj(vlc_instance_);

    // We assume that the access and its control are in the same process (as it should be).
    remote_stream_t remote_access =
    {
        access.object_id,
        access.port
    };

    std::printf("[DEMUXFACTORY] Remote access [access_id=%lu, port=%lu]\n",
            remote_access.object_id, remote_access.port);

    vlc_interrupt_t* it = interrupts_[interrupt_objectid];
    vlc_interrupt_set(it);

    stream_t *remote_stream_obj = vlc_stream_RemoteNew(instance_obj);
    remote_stream_obj->psz_url = strdup(filepath.c_str());

    vlc_rpc_ProxifyStream(remote_stream_obj, &remote_access, *demux_chan_);

    remote_esout_t remote_esout =
    {
        out.object_id,
        out.port
    };

    std::printf("[DEMUXFACTORY] Remote esout [esout_id=%lu, port=%lu]\n",
            remote_esout.object_id, remote_esout.port);

    es_out_t* remote_esout_obj = vlc_rpc_ProxifyEsOut(&remote_esout, *demux_chan_);

    std::printf("[DEMUXFACTORY] Proxified esout\n");
    std::fflush(stdout);

    demux_t* result = capi_vlc_demux_NewEx(instance_obj, module.c_str(), filepath.c_str(),
                                           remote_stream_obj, remote_esout_obj, preparsing);
    if(result == nullptr)
    {
        *demux_object = 0;
        return false;
    }
    *demux_object = demux_chan_->bind<Demux>(result, it);

    std::printf("[DEMUXFACTORY] Created demux id=%lu\n", *demux_object);

    return true;
}

bool DemuxFactory::stop()
{
    demux_chan_->stop();
    interrupt_chan_->stop();
    return true;
}

DemuxFactory::~DemuxFactory()
{
    capi_libvlc_release(vlc_instance_);
}
