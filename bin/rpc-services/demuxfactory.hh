#ifndef RPC_VLC_DEMUXFACTORY
#define RPC_VLC_DEMUXFACTORY

#include <vlc_interrupt.h>

#ifdef __cplusplus
extern "C" {
#endif
void start_factory(const char *demux_channel_handle,
                   int demux_port_val,
                   const char *interrupt_channel_handle,
                   int interrupt_port_val,
		   const char *shm_pool_str);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#include "demuxfactory.sidl.hh"
#include <map>

struct libvlc_instance_t;

class DemuxFactory : public vlc::DemuxFactoryReceiver
{
public:
    DemuxFactory(rpc::Channel* demux_chan, rpc::Channel* interrupt_chan);
    ~DemuxFactory();
    bool interrupt_create(std::uint64_t* it_object) override;
    bool demux_create(vlc::RemoteAccess access,
                      vlc::RemoteEsOut out,
                      std::string module,
                      std::string filepath,
                      bool preparsing,
                      std::uint64_t interrupt_objectid,
                      std::uint64_t *demux_object) override;
    bool stop() override;

private:
    rpc::Channel* demux_chan_;
    rpc::Channel* interrupt_chan_;
    libvlc_instance_t* vlc_instance_;
    std::map<rpc::ObjectId, vlc_interrupt_t*> interrupts_;
};

#endif
#endif
