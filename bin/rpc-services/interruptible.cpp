#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstdio>

#include <vlc_common.h>

#include <vlc_interrupt.h>

#include "interruptible.hh"

Interruptible::Interruptible(struct vlc_interrupt *it) : interrupt_(it) {}

bool Interruptible::interrupt(bool kill)
{
    std::printf("[INTERRUPTIBLE] interruption received: %s\n", kill ? "kill" : "raise");
    if (kill)
        vlc_interrupt_kill(interrupt_);
    else
        vlc_interrupt_raise(interrupt_);
    return true;
}

Interruptible::~Interruptible() { vlc_interrupt_destroy(interrupt_); }
