#ifndef RPC_VLC_INTERRUPTIBLE_HH
#define RPC_VLC_INTERRUPTIBLE_HH

#include "interruptible.sidl.hh"

class Interruptible : public vlc::InterruptibleReceiver
{
    public:
    explicit Interruptible(struct vlc_interrupt *it);
    bool interrupt(bool killed) final;

    ~Interruptible();

    private:
    struct vlc_interrupt *interrupt_;
};

#endif
