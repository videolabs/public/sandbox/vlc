#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "../lib/libvlc_internal.h"
#include <vlc/vlc.h>

#include "accessfactory.hh"

const char vlc_module_name[] = "accessservice";

/*
 * what we need:
 *
 * 1: the file descriptor for the access channel
 * 2: the access port id
 * 3-4: same for interruptions
 *
 * vlc-access-service <channel access fd> <port demux id> <broker port id> <broker object id>
 */

int main(int argc, char** argv)
{
    if (argc != 6)
    {
        printf("usage: %s <access channel fd> <access port id> <interrupt channel fd> <interrupt port id>\n", argv[0]);
        return 1;
    }

#ifdef TOP_BUILDDIR
    setenv ("VLC_PLUGIN_PATH", TOP_BUILDDIR"/modules", 1);
    setenv ("VLC_DATA_PATH", TOP_SRCDIR"/share", 1);
    setenv ("VLC_LIB_PATH", TOP_BUILDDIR"/modules", 1);
#endif

    // XXX: This will break if we use the full range of values as port id's are u64.
    int access_port_id = atoi(argv[2]);
    int interrupt_port_id = atoi(argv[4]);

    // Creates the channel, binds the factory object and starts the event loop.
    start_factory(argv[1], access_port_id, argv[3], interrupt_port_id, argv[5]);

    return 0;
}
