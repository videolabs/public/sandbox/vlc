#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <windows.h>

#include <vlc_common.h>
#include <vlc_spawn.h>

#include <iris_broker/iris_broker.h>
#include <iostream>
#include <cstdint>
#include <cassert>
#include <cstdio>

static uint64_t DuplicateStdOut()
{
#if defined(_WIN32)
    HANDLE out;
    BOOL result = DuplicateHandle(GetCurrentProcess(), (HANDLE)_get_osfhandle(1),
                                  GetCurrentProcess(), &out, 0, TRUE, DUPLICATE_SAME_ACCESS);
    assert(result);
    return reinterpret_cast<uint64_t>(out);
#else
    return dup(1);
#endif
}

int main()
{
    iris_enable_static_logger();
    HANDLE handle = ::CreateFileMappingA(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE, 0, 0x1000, "shm-test");
    uint8_t *map = (uint8_t*)::MapViewOfFile(handle, FILE_MAP_WRITE, 0, 0, 0);
    map[0] = 0x42;
    map[1] = 0x43;
    HANDLE dup;
    ::UnmapViewOfFile(map);
        DuplicateHandle(GetCurrentProcess(),
                    handle,
                    GetCurrentProcess(),
                    &dup,
                    0,
                    TRUE,
                    DUPLICATE_SAME_ACCESS);
    ::CloseHandle(handle);

    char handle_str[32] = {0};
    sprintf(handle_str, "%llu", (uint64_t)dup);

    IrisPolicyHandle policy = iris_policy_new_audit();

    IrisStatus status = iris_policy_allow_inherit_handle(policy, reinterpret_cast<uint64_t>(dup));
    assert(status == IrisStatus::Success);
    iris_policy_allow_dir_read(policy, "C:\\Users\\VLC\\Desktop\\sandbox\\plugins");
    iris_policy_allow_dir_lock(policy, "C:\\Users\\VLC\\Desktop\\sandbox\\plugins", false, true, true);

    const char *argv[] = {
        "C:\\Users\\VLC\\Desktop\\sandbox\\vlc-test-service.exe",
	handle_str,
    };
    IrisProcessConfigHandle process_config;
    status =
        iris_process_config_new(argv[0], 2, argv, &process_config);
    assert(status == IrisStatus::Success);

    uint64_t out = DuplicateStdOut();
    status = iris_process_config_redirect_stdout(process_config, out);
    assert(status == IrisStatus::Success);
    status = iris_process_config_redirect_stderr(process_config, out);
    assert(status == IrisStatus::Success);

    IrisWorkerHandle worker_handle;
    status = iris_worker_new(process_config, policy, &worker_handle);
    assert(status == IrisStatus::Success);
    OutputDebugString(L"HELLO\n");

    uint64_t pid;

    iris_worker_get_pid(worker_handle, &pid);
    if ((HANDLE)pid != INVALID_HANDLE_VALUE)
    	vlc_waitpid(pid);

    std::cerr.flush();
    std::cout.flush();
    //map = (uint8_t*)::MapViewOfFile(handle, FILE_MAP_READ, 0, 0, 0);
    //std::cerr << "host 1: " << (int)map[0] << " 2: " << (int)map[1] << "\n";
    //::UnmapViewOfFile(map);

    //iris_policy_free(policy);
    //iris_process_config_free(process_config);
    //iris_worker_free(worker_handle);
}
