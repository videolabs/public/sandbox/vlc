# libiris

LIBIRIS_GITURL := https://gitlab.com/libiris/libiris.git
LIBIRIS_HASH := dd817001b0e3bed8d31cd231214a3c118a5fe45a
LIBIRIS_BRANCH := main
LIBIRIS_VERSION := git-$(LIBIRIS_HASH)

ifdef BUILD_RUST
PKGS += libiris
endif

ifeq ($(call need_pkg,"libiris"),)
PKGS_FOUND += libiris
endif

ifdef HAVE_LINUX
DEPS_libiris = libseccomp
endif

#libiris_DEPS = cargo $(DEPS_cargo)

$(TARBALLS)/libiris-$(LIBIRIS_VERSION).tar.xz:
	$(call download_git,$(LIBIRIS_GITURL),$(LIBIRIS_BRANCH),$(LIBIRIS_HASH))

.sum-libiris: libiris-$(LIBIRIS_VERSION).tar.xz
	$(call check_githash,$(LIBIRIS_HASH))
	touch $@

libiris: libiris-$(LIBIRIS_VERSION).tar.xz .sum-libiris
	$(UNPACK)
	$(APPLY) $(SRC)/libiris/add-cargoc-support.patch
	$(APPLY) $(SRC)/libiris/bindings-broker-enable-lib-side-logging.patch
	$(APPLY) $(SRC)/libiris/bindings-worker-enable-lib-side-logging.patch
	$(APPLY) $(SRC)/libiris/bindgen-remove-enum-screaming-case.patch
	$(APPLY) $(SRC)/libiris/windows-broker-Enable-missing-winapi-features.patch
	$(APPLY) $(SRC)/libiris/windows-policy-Enable-missing-winapi-feature.patch
	$(APPLY) $(SRC)/libiris/windows-ipc-Fix-named-pipe-clashing.patch
ifdef HAVE_WIN32
	$(APPLY) $(SRC)/libiris/win32-spawn-in-broker-s-directory.patch
endif
	$(MOVE)

.libiris: libiris .cargo
	+export WINAPI_NO_BUNDLED_LIBRARIES=1                                     && \
	cd $< && $(CARGOC_INSTALL) --no-default-features $(LIBIRIS_FEATURES)      && \
	sed -i -e 's/-lwindows.0.48.5 //g' $(PREFIX)/lib/pkgconfig/iris_broker.pc && \
	sed -i -e 's/-lwindows.0.48.5 //g' $(PREFIX)/lib/pkgconfig/iris_worker.pc && \
	sed -i -e 's/-lopengl32/-lntdll/g' $(PREFIX)/lib/pkgconfig/iris_broker.pc && \
	sed -i -e 's/-lopengl32/-lntdll/g' $(PREFIX)/lib/pkgconfig/iris_worker.pc
	touch $@
