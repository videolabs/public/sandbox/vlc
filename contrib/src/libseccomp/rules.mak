SECCOMP_VERSION := 2.5.4
SECCOMP_URL := $(GITHUB)/seccomp/libseccomp/archive/refs/tags/v$(SECCOMP_VERSION).tar.gz

ifeq ($(call need_pkg,"libseccomp >= 2.0.0"),)
PKGS_FOUND += libseccomp
endif
PKGS += libseccomp


$(TARBALLS)/libseccomp-$(SECCOMP_VERSION).tar.gz:
	$(call download_pkg,$(SECCOMP_URL),libseccomp)

.sum-libseccomp: libseccomp-$(SECCOMP_VERSION).tar.gz

libseccomp: libseccomp-$(SECCOMP_VERSION).tar.gz .sum-libseccomp
	$(UNPACK)
	$(MOVE)

.libseccomp: libseccomp
	$(RECONF)
	cd $< && $(HOSTVARS) ./configure $(HOSTCONF)
	cd $< && $(MAKE) install
	touch $@
