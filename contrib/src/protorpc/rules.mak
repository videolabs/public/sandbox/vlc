PROTORPC_HASH := 9b73828e5a94f3f953b387efb20d53d7b590a5bd
PROTORPC_VERSION := git-$(PROTORPC_HASH)
PROTORPC_BRANCH := win32-shm-pool
PROTORPC_GITURL := https://gitlab.com/videolabs/public/sandbox/protorpc.git

PKGS += protorpc

$(TARBALLS)/protorpc-$(PROTORPC_VERSION).tar.xz:
	$(call download_git,$(PROTORPC_GITURL),$(PROTORPC_BRANCH),$(PROTORPC_HASH))

.sum-protorpc: protorpc-$(PROTORPC_VERSION).tar.xz
	$(call check_githash,$(PROTORPC_HASH))

protorpc: protorpc-$(PROTORPC_VERSION).tar.xz .sum-protorpc
	$(UNPACK)
	$(APPLY) $(SRC)/protorpc/change-named-pipe-prefix.patch
	$(MOVE)

.protorpc: protorpc crossfile.meson
	$(MESONCLEAN)
	$(HOSTVARS_MESON) $(MESON) -Dsidl_vlc_contrib=true -Dprotoipc_tests=false -Dprotorpc_tests=false
	+$(MESONBUILD)
	touch $@
