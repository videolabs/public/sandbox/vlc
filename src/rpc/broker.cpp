#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <charconv>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <filesystem>
#include <memory>
#include <mutex>
#include <optional>
#include <set>
#include <stdarg.h>
#include <stdexcept>
#include <string>
#include <thread>
#include <unistd.h>
#include <utility>
#include <vector>

#include <vlc_common.h>

#include <vlc_block.h>
#include <vlc_demux.h>
#include <vlc_dialog.h>
#include <vlc_es.h>
#include <vlc_es_out.h>
#include <vlc_frame.h>
#include <vlc_interrupt.h>
#include <vlc_modules.h>
#include <vlc_spawn.h>
#include <vlc_stream.h>
#include <vlc_tick.h>
#include <vlc_url.h>

#include <iris_broker/iris_broker.h>

#include <protoipc/port.hh>
#include <protoipc/shm.hh>
#include <protoipc/router.hh>
#include <protorpc/channel.hh>
#include <protorpc/rpcobject.hh>

#include <rpc/demux.sidl.hh>
#include <rpc/demuxfactory.sidl.hh>
#include <rpc/esout.sidl.hh>
#include <rpc/interruptible.sidl.hh>
#include <rpc/stream.sidl.hh>
#include <rpc/streamfactory.sidl.hh>

#include "../modules/modules.h"
#include "broker.h"
#include "esout.hh"
#include "proxify.hh"

struct SandboxedProcess {
    IrisPolicyHandle policy;
    IrisProcessConfigHandle worker_config;
    IrisWorkerHandle worker;
};

template <typename ProxyImpl>
struct ServiceFactory {
    ipc::PortId port_id;
    rpc::Proxy<ProxyImpl> proxy;

    ipc::PortId interrupt_port_id;

    SandboxedProcess sandboxed_process;

    ServiceFactory(libvlc_int_t *vlc,
                   const char *service_name,
                   const std::set<std::string_view> &allowed_files_read,
		   ipc::PortId channel_to,
		   std::unique_ptr<ipc::Port> &&channel_from,
		   ipc::PortId interrupt_to,
		   std::unique_ptr<ipc::Port> &&interrupt_from);
    ~ServiceFactory();
};

using AccessFactory = ServiceFactory<vlc::StreamFactoryProxy>;
using DemuxFactory = ServiceFactory<vlc::DemuxFactoryProxy>;

struct InterruptContext {
    vlc_interrupt_t *context;
    rpc::ObjectId access_id = 0;
    rpc::Proxy<vlc::InterruptibleProxy> access_proxy = nullptr;
    rpc::ObjectId demux_id = 0;
    rpc::Proxy<vlc::InterruptibleProxy> demux_proxy = nullptr;
};

struct broker_priv_t {
    std::mutex mtx;
    std::uint32_t rc;
    std::unique_ptr<ipc::Router> broker_router;
    std::thread broker_router_thread;
    std::unique_ptr<rpc::Channel> broker_channel;
    std::thread broker_channel_thread;
    std::unique_ptr<rpc::Channel> esout_channel;
    ipc::PortId esout_portid;
    std::thread esout_thread;
    std::unique_ptr<rpc::Channel> interrupt_channel;
    std::thread interrupt_thread;

    ipc::PortId access_channel_to;
    std::unique_ptr<ipc::Port> access_channel_from;
    ipc::PortId access_interrupt_to;
    std::unique_ptr<ipc::Port> access_interrupt_from;
    std::unique_ptr<AccessFactory> access_factory;
    ipc::PortId demux_channel_to;
    std::unique_ptr<ipc::Port> demux_channel_from;
    ipc::PortId demux_interrupt_to;
    std::unique_ptr<ipc::Port> demux_interrupt_from;
    std::unique_ptr<DemuxFactory> demux_factory;

    std::atomic_bool dialog_enabled;
};

namespace
{ // XXX: ODR protection
static broker_priv_t priv;

std::mutex remote_stream_lock_;
bool connection_closed_ = false;
}

static bool resolve_helper_path(const char *program_name, std::string &output)
{
#if defined(__linux__)
    std::string full_progname(program_name);
    char path_buff[4096];
    ssize_t count = readlink("/proc/self/exe", path_buff, sizeof(path_buff));

    if (count < 0)
        return false;

    // readlink does not append a null byte
    path_buff[count] = 0;
#else
    std::string full_progname = std::string(program_name) + ".exe";
    WCHAR path_buff[MAX_PATH];
    GetModuleFileNameW(NULL, path_buff, MAX_PATH);
#endif
    std::filesystem::path bin_path(path_buff);
    std::filesystem::path bin_folder = bin_path.parent_path();
    std::filesystem::path prog_path = bin_folder / full_progname;

    if (!std::filesystem::exists(prog_path))
    {
        std::cerr << "Cannot find executable named " << prog_path << std::endl;
        return false;
    }

    auto status = std::filesystem::status(prog_path);

    if (status.type() != std::filesystem::file_type::regular)
        return false;

    auto perms = status.permissions();

    if ((perms & std::filesystem::perms::owner_exec) == std::filesystem::perms::none)
        return false;

    output = prog_path.string();

    return true;
}

static void iris_policy_allow_protorpc_handle(IrisPolicyHandle policy, const ipc::Handle &handle)
{
#if defined(_WIN32)
    IrisStatus status = iris_policy_allow_inherit_handle(policy, reinterpret_cast<uint64_t>(handle.write));
    assert(status == IrisStatus::Success);
    status = iris_policy_allow_inherit_handle(policy, reinterpret_cast<uint64_t>(handle.read));
    assert(status == IrisStatus::Success);
#else
    IrisStatus status = iris_policy_allow_inherit_handle(policy, handle.handle);
    assert(status == IrisStatus::Success);
#endif
}

static uint64_t DuplicateStdOut()
{
#if defined(_WIN32)
    HANDLE out;
    BOOL result = DuplicateHandle(GetCurrentProcess(), (HANDLE)_get_osfhandle(1),
                                  GetCurrentProcess(), &out, 0, TRUE, DUPLICATE_SAME_ACCESS);
    assert(result);
    return reinterpret_cast<uint64_t>(out);
#else
    return dup(1);
#endif
}

static void
IrisLogCallback(const IrisPolicyRequest *rq, const IrisPolicyVerdict *verdict, const void *opaque)
{
    if (!priv.dialog_enabled)
        return;

    auto *obj = static_cast<vlc_object_t*>(const_cast<void*>(opaque));
    switch(verdict->tag)
    {
        case IrisPolicyVerdict::Tag::DelegationToSandboxNotSupported:
        case IrisPolicyVerdict::Tag::DeniedByPolicy:
            if (rq->tag == IrisPolicyRequest::Tag::FileOpen)
                vlc_dialog_display_error(obj, "Sandbox violation", "File access to \"%s\" denied", rq->file_open.path);
#if defined(__linux__)
            else if (rq->tag == IrisPolicyRequest::Tag::Syscall)
                vlc_dialog_display_error(obj, "Sandbox violation", "Syscall \"%" PRIi64 "\" denied", rq->syscall.nb);
#elif defined(_WIN32)
            else if (rq->tag == IrisPolicyRequest::Tag::RegKeyOpen)
                vlc_dialog_display_error(obj, "Sandbox violation", "Register key \"%s\" access denied", rq->reg_key_open.path);
#endif
        default: break;
    }
}

static std::string handles_to_string(const std::vector<ipc::shm::HandlePtr> &handles)
{
    std::string ret;
    for (auto i = 0u; i < handles.size(); ++i)
    {
        ret += std::to_string(reinterpret_cast<intptr_t>(handles[i].get()));
        if (i + 1 != handles.size())
            ret += ':';
    }
    return ret;
}

/*
 * Creates a new process and returns its pid.
 * \param port_id The child process channel port ID.
 * \param port_handle The child port connected to the broker.
 * \param interrupt_port_id The interrupt channel port id.
 * \param interrupt_port_handle The interrupt port connected to the broker.
 */
static SandboxedProcess
vlc_broker_CreateProcess(libvlc_int_t *vlc,
                         const char *program_name,
                         ipc::PortId port_id,
                         Handle port_handle,
                         ipc::PortId interrupt_port_id,
                         Handle interrupt_port_handle,
                         const std::set<std::string_view> allowed_files_read)
{
    std::string bin_path;

    if (!resolve_helper_path(program_name, bin_path))
        throw std::runtime_error("Can't process path");

    char channel_port_str[32] = {0};
    std::to_chars(channel_port_str, channel_port_str + sizeof(channel_port_str),
                  port_id);

    char interrupt_channel_port_str[32] = {0};
    std::to_chars(interrupt_channel_port_str,
                  interrupt_channel_port_str + sizeof(interrupt_channel_port_str),
                  interrupt_port_id);

    const auto port_handle_str = port_handle.desc();
    const auto interrupt_port_handle_str = interrupt_port_handle.desc();
    const auto shm_pool = ipc::shm::duplicate_pool();
    const std::string shm_pool_str = handles_to_string(shm_pool);

    const char *argv[] = {
        bin_path.c_str(),
        port_handle_str.c_str(),
        channel_port_str,
        interrupt_port_handle_str.c_str(),
        interrupt_channel_port_str,
        shm_pool_str.c_str(),
    };

    IrisPolicyHandle policy = iris_policy_new_audit();

    iris_policy_add_log_callback(policy, IrisLogCallback, vlc);

    for (const auto allowed : allowed_files_read)
    {
        iris_policy_allow_file_read(policy, allowed.data());
        iris_policy_allow_file_lock(policy, allowed.data(), false, true, true);
    }

    iris_policy_allow_protorpc_handle(policy, port_handle);
    iris_policy_allow_protorpc_handle(policy, interrupt_port_handle);

    for (const auto &dup : shm_pool)
    {
    	IrisStatus status = iris_policy_allow_inherit_handle(policy, reinterpret_cast<uint64_t>(dup.get()));
    	assert(status == IrisStatus::Success);
    }

    char *plugin_path = ::getenv( "VLC_PLUGIN_PATH" );
    if (plugin_path != nullptr)
    {
        iris_policy_allow_dir_read(policy, plugin_path);
        iris_policy_allow_dir_lock(policy, plugin_path, false, true, true);
        ::free(plugin_path);
    }

    IrisProcessConfigHandle process_config;
    IrisStatus status =
        iris_process_config_new(bin_path.c_str(), ARRAY_SIZE(argv), argv, &process_config);
    assert(status == IrisStatus::Success);

    uint64_t out = DuplicateStdOut();
    status = iris_process_config_redirect_stdout(process_config, out);
    assert(status == IrisStatus::Success);
    status = iris_process_config_redirect_stderr(process_config, out);
    assert(status == IrisStatus::Success);

    IrisWorkerHandle worker_handle;
    status = iris_worker_new(process_config, policy, &worker_handle);
    assert(status == IrisStatus::Success);

    return SandboxedProcess{policy, process_config, worker_handle};
}

template <typename ProxyImpl>
ServiceFactory<ProxyImpl>::ServiceFactory(libvlc_int_t *vlc,
                                          const char *service_name,
                                          const std::set<std::string_view> &allowed_files_read,
                                          ipc::PortId channel_to,
                                          std::unique_ptr<ipc::Port> &&channel_from,
                                          ipc::PortId interrupt_to,
                                          std::unique_ptr<ipc::Port> &&interrupt_from)
{
    port_id = channel_to;
    interrupt_port_id = interrupt_to;
    // XXX: Duplicate the ports because protorpcs forces FD_CLOEXEC on the ports in the port
    // pair. Duplicating it now will allows the subprocess to use it.
    const Handle service_port_handle = channel_from->dup();
    const Handle interrupt_port_handle = interrupt_from->dup();

    msg_Info(vlc, "Spawning worker \"%s\"", service_name);
    sandboxed_process =
        vlc_broker_CreateProcess(vlc, service_name, port_id, service_port_handle,
                                 interrupt_port_id, interrupt_port_handle, allowed_files_read);
    channel_from->close();
    interrupt_from->close();

    // We connect to the remote process ObjectFactory
    proxy = priv.broker_channel->connect<ProxyImpl>(port_id, 0);
}

template <typename ProxyImpl>
ServiceFactory<ProxyImpl>::~ServiceFactory()
{
    proxy->stop();

    uint64_t pid;
    iris_worker_get_pid(sandboxed_process.worker, &pid);
    vlc_waitpid(pid);

    iris_policy_free(sandboxed_process.policy);
    iris_process_config_free(sandboxed_process.worker_config);
    iris_worker_free(sandboxed_process.worker);
}

/*
 * Stream apis.
 */

struct remote_stream_sys_t
{
    rpc::Proxy<vlc::StreamProxy> proxy;
};

static ssize_t vlc_RemoteStream_Read(stream_t *s, void *buf, size_t len)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto *sys = reinterpret_cast<remote_stream_sys_t *>(s->p_sys);

    std::printf("[STREAM-PROXY] Read request to #%lu\n", sys->proxy->remote_id());
    std::vector<std::uint8_t> data;
    int64_t status = 0;

    if (!sys->proxy->read(len, &status, &data))
        return -1;

    if (status >= 0)
        std::memcpy(buf, data.data(), data.size());

    return status;
}

static int vlc_RemoteStream_Seek(stream_t* s, uint64_t offset)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto *sys = reinterpret_cast<remote_stream_sys_t *>(s->p_sys);

    std::printf("[STREAM-PROXY] Seek request to #%lu\n", sys->proxy->remote_id());
    int32_t status = 0;

    if (!sys->proxy->seek(offset, &status))
        return -1;

    return status;
}

static block_t* vlc_RemoteStream_Block(stream_t* s, bool* out_eof)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto *sys = reinterpret_cast<remote_stream_sys_t *>(s->p_sys);

    std::printf("[STREAM-PROXY] Block #%lu\n", sys->proxy->remote_id());
    std::optional<vlc::Block> block;
    std::uint8_t eof = 0;

    if (!sys->proxy->block(&eof, &block))
        return NULL;

    *out_eof = eof;

    if (!block)
        return NULL;

    block_t* result = block_Alloc(block->buffer.size());
    result->i_flags = block->flags;
    result->i_nb_samples = block->nb_samples;
    result->i_pts = block->pts;
    result->i_dts = block->dts;
    result->i_length = block->length;

    std::memcpy(result->p_buffer, block->buffer.data(), block->buffer.size());

    return result;
}

void vlc_RemoteStream_Destroy(stream_t* s)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto *sys = reinterpret_cast<remote_stream_sys_t *>(s->p_sys);

    std::printf("[STREAM-PROXY] Destroy #%lu\n", sys->proxy->remote_id());
    sys->proxy->destroy();
    delete sys;
}

static int vlc_RemoteStream_Readdir(stream_t* s, input_item_node_t*)
{
    std::printf("[STREAM-PROXY] Stream %p tried to call Readdir\n", s);
    return -1;
}

static int vlc_RemoteStream_Demux(stream_t* s)
{
    std::printf("[STREAM-PROXY] Stream %p tried to call Demux\n", s);
    return -1;
}

static int vlc_RemoteStream_Control(stream_t* s, int cmd, va_list args)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    const auto &stream = reinterpret_cast<remote_stream_sys_t *>(s->p_sys)->proxy;

    std::int64_t ret = VLC_EGENERIC;

    switch (cmd)
    {
        case STREAM_CAN_SEEK:
        {
            bool* result = va_arg(args, bool*);

            if (!stream->control_can_seek(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_CAN_FASTSEEK:
        {
            bool* result = va_arg(args, bool*);

            if (!stream->control_can_fastseek(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_CAN_PAUSE:
        {
            bool* result = va_arg(args, bool*);

            if (!stream->control_can_pause(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_CAN_CONTROL_PACE:
        {
            bool* result = va_arg(args, bool*);

            if (!stream->control_can_control_pace(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_GET_SIZE:
        {
            std::uint64_t* result = va_arg(args, std::uint64_t*);

            if (!stream->control_get_size(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_GET_PTS_DELAY:
        {
            vlc_tick_t* result = va_arg(args, vlc_tick_t*);

            if (!stream->control_get_pts_delay(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_SET_PAUSE_STATE:
        {
            int state = va_arg(args, int);

            if (!stream->control_set_pause_state(state, &ret))
                return VLC_EGENERIC;

            return ret;
        }
        case STREAM_GET_CONTENT_TYPE:
        {
            char** result = va_arg(args, char**);
            std::optional<std::string> content_type;

            if (!stream->control_get_content_type(&ret, &content_type))
                return VLC_EGENERIC;

            if (content_type)
                *result = strdup(content_type->c_str());
            else
                *result = NULL;

            return ret;
        }
        case STREAM_SET_SEEKPOINT:
        {
            int seek = va_arg(args, int);

            if (!stream->control_set_seekpoint(seek, &ret))
                return VLC_EGENERIC;

            return ret;
        }
        default:
            std::printf("[CONTROL-PROXY] Unhandled command: %i\n", cmd);
            return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

// Attempts to recover the rpc objects associated with a proxyfied stream.
static rpc::Proxy<vlc::StreamProxy> stream_get_access_proxies(stream_t *s)
{
    // Seek to the last stream (should be the proxyfied access).
    while (s->s)
        s = s->s;

    // vlc_stream_proxy_objects is stored in stream_t::p_sys
    if (!s->p_sys)
        return nullptr;

    // Proxyfied streams must have the proxyfied shim functions
    if (s->pf_read != &vlc_RemoteStream_Read)
        return nullptr;

    return static_cast<remote_stream_sys_t*>(s->p_sys)->proxy;
}

int vlc_broker_Init()
{
    std::lock_guard lock(priv.mtx);
    if(priv.rc > 0)
    {
        priv.rc++;
        return 0;
    }

    iris_enable_static_logger();

    priv.broker_router = ipc::Router::make();
    auto [broker_to, broker_from] = priv.broker_router->create_port();
    auto [esout_to, esout_from] = priv.broker_router->create_port();
    priv.esout_portid = esout_to;
    auto [interrupt_to, interrupt_from] = priv.broker_router->create_port();
    std::tie(priv.access_channel_to, priv.access_channel_from) = priv.broker_router->create_port();
    std::tie(priv.access_interrupt_to, priv.access_interrupt_from) = priv.broker_router->create_port();
    std::tie(priv.demux_channel_to, priv.demux_channel_from) = priv.broker_router->create_port();
    std::tie(priv.demux_interrupt_to, priv.demux_interrupt_from) = priv.broker_router->create_port();

    priv.broker_channel = std::make_unique<rpc::Channel>(broker_to, std::move(broker_from));
    priv.esout_channel = std::make_unique<rpc::Channel>(esout_to, std::move(esout_from));
    priv.interrupt_channel =
        std::make_unique<rpc::Channel>(interrupt_to, std::move(interrupt_from));

    priv.broker_router_thread = std::thread([]() {
        ipc::PortError error = priv.broker_router->loop();
        if(error == ipc::PortError::Ok)
            return;

        // If we reached here an unrecoverable error occured
        if (error == ipc::PortError::PollError)
            throw std::runtime_error("ipc router: polling error");
        else if(error == ipc::PortError::ConnectionClosed)
        {
            std::perror("ipc router: Socket closed by worker");
            priv.esout_channel->stop();
            priv.broker_channel->stop();
            priv.interrupt_channel->stop();
            std::lock_guard<std::mutex> lock(remote_stream_lock_);
            connection_closed_ = true;
        }
        else if (error == ipc::PortError::BadFileDescriptor)
            throw std::runtime_error("ipc router: bad file descriptor");
        else
            throw std::runtime_error("ipc router: Unknown error");
    });

    priv.broker_channel_thread = std::thread([]() {
        priv.broker_channel->loop();
    });

    priv.esout_thread = std::thread([]() {
        priv.esout_channel->loop();
    });

    priv.interrupt_thread = std::thread([]() {
        priv.interrupt_channel->loop();
    });

    priv.rc++;
    priv.dialog_enabled = false;

    return 0;
}

void vlc_broker_EnableDialogs(bool enable)
{
    priv.dialog_enabled = enable;
}

int vlc_broker_Deinit()
{
    {
        std::lock_guard lock(priv.mtx);
        if(--priv.rc > 0)
            return 1;
    }

    priv.demux_factory = nullptr;
    priv.access_factory = nullptr;

    priv.esout_channel->stop();
    priv.broker_channel->stop();
    priv.interrupt_channel->stop();
    priv.esout_thread.join();
    priv.broker_channel_thread.join();
    priv.interrupt_thread.join();

    priv.broker_router->terminate();
    priv.broker_router_thread.join();
    return 0;
}

void vlc_rpc_ProxifyStream(stream_t* local, remote_stream_t* remote, rpc::Channel& chan)
{
    local->p_sys =
        new remote_stream_sys_t{chan.connect<vlc::StreamProxy>(remote->port, remote->object_id)};

    // Install hooks
    local->pf_read = vlc_RemoteStream_Read;
    local->pf_block = vlc_RemoteStream_Block;
    local->pf_readdir = vlc_RemoteStream_Readdir;
    local->pf_demux = vlc_RemoteStream_Demux;
    local->pf_seek = vlc_RemoteStream_Seek;
    local->pf_control = vlc_RemoteStream_Control;
}

static auto broker_GetSharedDependencies(const char *cap, const char *module) {
    std::set<std::string_view> ret;

    module_t **list;
    const ssize_t n = vlc_module_match(cap, module, false, &list, NULL);

    for (ssize_t i = 0; i < n; ++i)
    {
        const module_t *mod = list[i];
        const char *so_name;
        vlc_vector_foreach(so_name, &mod->plugin->dl_deps) { ret.emplace(so_name); }
    }
    free(list);

    return ret;
}

int vlc_broker_CreateAccess(stream_t* s)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);

    if (priv.access_factory == nullptr)
    {
        msg_Dbg(s, "[BROKER] creating vlc-access-service");
        auto deps = broker_GetSharedDependencies("access", s->psz_name);
        char *path = vlc_uri2path(s->psz_url);
        deps.emplace(path);

        try
        {
            priv.access_factory =
                std::make_unique<AccessFactory>(vlc_object_instance(s), "vlc-access-service", deps, priv.access_channel_to, std::move(priv.access_channel_from), priv.access_interrupt_to, std::move(priv.access_interrupt_from));
        }
        catch (const std::runtime_error &)
        {
            free(path);
            return -1;
        }
        free(path);
    }

    // Now we need to create the access object itself
    auto& access_factory = priv.access_factory->proxy;
    const ipc::PortId access_factory_port = priv.access_factory->port_id;

    rpc::ObjectId access_object = 0;

    auto *interrupt_context = static_cast<InterruptContext *>(vlc_interrupt_getdata());
    assert(interrupt_context != nullptr);
    if (interrupt_context->access_proxy == nullptr)
    {
        if (!access_factory->interrupt_create(&interrupt_context->access_id))
        {
            std::printf("[BROKER] Call to AccessFactory::interrupt_create(...) failed\n");
            return -1;
        }
        const ipc::PortId access_interrupt_port = priv.access_factory->interrupt_port_id;
        interrupt_context->access_proxy = priv.interrupt_channel->connect<vlc::InterruptibleProxy>(
            access_interrupt_port, interrupt_context->access_id);
    }

    if (!access_factory->access_create(s->psz_url, s->b_preparsing, interrupt_context->access_id,
                                       &access_object))
    {
        std::printf("[BROKER] Call to DemuxFactory::demux_create(...) failed\n");
        return -1;
    }

    if(access_object == 0)
    {
        std::printf("[BROKER] Creation of new access failed\n");
        return -1;
    }

    std::printf("[BROKER] Created access for url: %s (port: %lu, object id: %lu)\n",
            s->psz_url, access_factory_port, access_object);

    remote_stream_t remote_info;
    remote_info.port = access_factory_port;
    remote_info.object_id = access_object;

    // Install the rpc proxies on the current channel and bind them to the C object.
    vlc_rpc_ProxifyStream(s, &remote_info, *priv.broker_channel);

    return 0;
}

// EsOut proxification

struct vlc_es_out_proxy_object
{
    struct es_out_t out;
    rpc::Proxy<vlc::EsOutProxy> object;
};

static es_out_id_t *vlc_RemoteEsOut_Add(es_out_t *out, input_source_t *, const es_format_t *fmt)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto* esout = reinterpret_cast<vlc_es_out_proxy_object*>(out);
    auto& remote = esout->object;
    std::uint64_t fake_es_out_id = 0;

    if (!fmt)
    {
        std::printf("[ESOUT-PROXY] Add(<empty fmt>)\n");
        if (!remote->add({}, &fake_es_out_id))
            return NULL;

        return reinterpret_cast<es_out_id_t*>(fake_es_out_id);
    }

    // Convert es_format_t into vlc::EsFormat
    vlc::EsFormat out_fmt;

    out_fmt.raw_struct.resize(sizeof(*fmt));
    std::memcpy(out_fmt.raw_struct.data(), fmt, sizeof(*fmt));

    if (fmt->p_extra_languages && fmt->i_extra_languages > 0)
    {
        std::vector<std::uint8_t> extra_languages;
        extra_languages.resize(fmt->i_extra_languages);
        std::memcpy(extra_languages.data(), fmt->p_extra_languages, extra_languages.size());

        out_fmt.extra_languages = std::move(extra_languages);
    }

    if (fmt->p_extra && fmt->i_extra > 0)
    {
        std::vector<std::uint8_t> extra;
        extra.resize(fmt->i_extra);
        std::memcpy(extra.data(), fmt->p_extra, extra.size());

        out_fmt.extra = std::move(extra);
    }

    std::printf("[ESOUT-PROXY] Add(<some fmt>)\n");

    if (!remote->add(out_fmt, &fake_es_out_id))
        return NULL;

    std::printf("[ESOUT-PROXY] Received fake_es_out_id: %lu\n", fake_es_out_id);

    return reinterpret_cast<es_out_id_t*>(fake_es_out_id);
}

static int vlc_RemoteEsOut_Send(es_out_t *out, es_out_id_t *id, block_t *block)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto* esout = reinterpret_cast<vlc_es_out_proxy_object*>(out);
    auto& remote = esout->object;
    std::uint64_t fake_es_out_id = reinterpret_cast<std::uint64_t>(id);
    std::int32_t ret = VLC_EGENERIC;

    if (!block)
    {
        std::printf("[ESOUT-PROXY] Send(<empty block>)\n");
        remote->send(fake_es_out_id, {}, &ret);
        return ret;
    }

    vlc::EsBlock out_block;
    std::vector<std::uint8_t> data(block->p_buffer, block->p_buffer + block->i_buffer);

    out_block.buffer = std::move(data);
    out_block.flags = block->i_flags;
    out_block.nb_samples = block->i_nb_samples;
    out_block.pts = block->i_pts;
    out_block.dts = block->i_dts;
    out_block.length = block->i_length;

    std::printf("[ESOUT-PROXY] Send(block size=%lu)\n", block->i_size);

    block_Release(block);

    remote->send(fake_es_out_id, out_block, &ret);
    return ret;
}

static void vlc_RemoteEsOut_Del(es_out_t *out, es_out_id_t *id)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto* esout = reinterpret_cast<vlc_es_out_proxy_object*>(out);
    auto& remote = esout->object;
    std::uint64_t fake_es_out_id = reinterpret_cast<std::uint64_t>(id);

    std::printf("[ESOUT-PROXY] Del(es_out_id = %lu)\n", fake_es_out_id);
    remote->del(reinterpret_cast<std::uint64_t>(id));
}

static int vlc_RemoteEsOut_Control(es_out_t *out, input_source_t *, int query, va_list args)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto* esout = reinterpret_cast<vlc_es_out_proxy_object*>(out);
    auto& remote = esout->object;
    std::int64_t ret = VLC_EGENERIC;

    switch (query)
    {
        case ES_OUT_SET_PCR:
        {
            std::int64_t i_pcr = va_arg(args, std::int64_t);

            if (!remote->control_set_pcr(i_pcr, &ret))
                return VLC_EGENERIC;

            return ret;
        }
        case ES_OUT_SET_NEXT_DISPLAY_TIME:
        {
            std::int64_t i_pts = va_arg(args, std::int64_t);

            if (!remote->control_set_next_display_time(i_pts, &ret))
                return VLC_EGENERIC;

            return ret;
        }
        case ES_OUT_GET_ES_STATE:
        {
            std::uint64_t fake_es_out_id = va_arg(args, std::uint64_t);
            bool* state = va_arg(args, bool*);

            if (!remote->control_get_es_state(fake_es_out_id, &ret, state))
                return VLC_EGENERIC;

            return ret;
        }
        default:
            std::printf("[ESOUT-PROXY] Stubbed control command = %i\n", query);
            break;
    }

    return VLC_EGENERIC;
}

static void vlc_RemoteEsOut_Destroy(es_out_t *out)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    std::printf("[ESOUT-PROXY] Destroy()\n");
    auto* esout = reinterpret_cast<vlc_es_out_proxy_object*>(out);
    auto& remote = esout->object;
    remote->destroy();
    delete esout;
}

static int vlc_RemoteEsOut_PrivControl(es_out_t *, input_source_t *, int query, va_list)
{
    std::printf("[ESOUT-PROXY] Stubbed priv_control command = %i\n", query);
    return VLC_EGENERIC;
}

static const struct es_out_callbacks es_out_proxy_cbs =
{
    vlc_RemoteEsOut_Add,
    vlc_RemoteEsOut_Send,
    vlc_RemoteEsOut_Del,
    vlc_RemoteEsOut_Control,
    vlc_RemoteEsOut_Destroy,
    vlc_RemoteEsOut_PrivControl,
};

es_out_t* vlc_rpc_ProxifyEsOut(remote_esout_t* remote, rpc::Channel& chan)
{
    vlc_es_out_proxy_object* out = new vlc_es_out_proxy_object;
    out->out.cbs = &es_out_proxy_cbs;
    out->object = chan.connect<vlc::EsOutProxy>(remote->port, remote->object_id);

    return reinterpret_cast<es_out_t*>(out);
}

// Demux proxification

struct remote_demux_sys_t {
    rpc::Proxy<vlc::DemuxProxy> proxy;
};

static int vlc_RemoteDemux_Readdir(stream_t *, input_item_node_t *)
{
    std::printf("[DEMUX-PROXY] Stubbed Readdir\n");
    return VLC_EGENERIC;
}

static int vlc_RemoteDemux_Demux(stream_t *s)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    std::printf("[DEMUX-PROXY] Demux()\n");
    if(connection_closed_)
        return VLC_DEMUXER_EOF;

    auto* sys = reinterpret_cast<remote_demux_sys_t*>(s->p_sys);
    std::int32_t result = 0;

    if (!sys->proxy->demux(&result))
        return VLC_EGENERIC;

    return result;
}

static int vlc_RemoteDemux_Control(stream_t *s, int cmd, va_list args)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    const auto& demux = reinterpret_cast<remote_demux_sys_t*>(s->p_sys)->proxy;

    std::int64_t ret = VLC_EGENERIC;

    switch (cmd)
    {
        case DEMUX_CAN_SEEK:
        {
            bool* result = va_arg(args, bool*);

            if (!demux->control_can_seek(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_CAN_PAUSE:
        {
            bool* result = va_arg(args, bool*);

            if (!demux->control_can_pause(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_CAN_CONTROL_PACE:
        {
            bool* result = va_arg(args, bool*);

            if (!demux->control_can_control_pace(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_GET_PTS_DELAY:
        {
            vlc_tick_t* result = va_arg(args, vlc_tick_t*);

            if (!demux->control_get_pts_delay(&ret, result))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_SET_PAUSE_STATE:
        {
            int state = va_arg(args, int);

            if (!demux->control_set_pause_state(state, &ret))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_TEST_AND_CLEAR_FLAGS:
        {
            unsigned* flags = va_arg(args, unsigned*);

            if (!demux->control_test_and_clear_flags(*flags, &ret, flags))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_GET_TIME:
        {
            vlc_tick_t* ticks = va_arg(args, vlc_tick_t*);

            if (!demux->control_get_time(&ret, ticks))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_GET_LENGTH:
        {
            vlc_tick_t* ticks = va_arg(args, vlc_tick_t*);

            if (!demux->control_get_length(&ret, ticks))
                return VLC_EGENERIC;

            return ret;
        }
        case DEMUX_GET_NORMAL_TIME:
        {
            vlc_tick_t* ticks = va_arg(args, vlc_tick_t*);

            if (!demux->control_get_normal_time(&ret, ticks))
                return VLC_EGENERIC;

            return ret;
        }
        default:
            std::printf("[DEMUX-CONTROL-PROXY] Unhandled command: %i\n", cmd);
            return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

void vlc_RemoteDemux_Destroy(stream_t *s)
{
    es_out_Delete(s->out);

    assert(s->s != NULL);
    vlc_stream_Delete(s->s);

    auto *sys = static_cast<remote_demux_sys_t *>(s->p_sys);
    delete sys;
}

void vlc_rpc_ProxifyDemux(demux_t* local, remote_demux_t* remote, rpc::Channel& chan)
{
    local->p_sys =
        new remote_demux_sys_t{chan.connect<vlc::DemuxProxy>(remote->port, remote->object_id)};

    // Access should already be proxified.
    assert(local->s->pf_read == vlc_RemoteStream_Read);

    local->out = vlc_rpc_ProxifyEsOut(&remote->esout, chan);

    local->pf_readdir = vlc_RemoteDemux_Readdir;
    local->pf_demux = vlc_RemoteDemux_Demux;
    local->pf_control = vlc_RemoteDemux_Control;
}

int vlc_broker_CreateDemux(demux_t* demux, const char* module, const char* url)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);

    // Step 1: Create a esout rpc object for the demux.
    rpc::ObjectId esout_id = priv.esout_channel->bind<EsOut>(demux->out);

    // Step 2: Recover access rpc objects
    const auto stream_proxy = stream_get_access_proxies(demux->s);

    if (stream_proxy == nullptr)
    {
        std::printf("[BROKER] Trying to create demux using non proxyfied access\n");
        return -1;
    }

    std::printf("[BROKER] Using remote stream [object=(%lu,%lu) for demux\n",
            stream_proxy->remote_port(),
            stream_proxy->remote_id());

    // Step 3: Initialize remote demux factory.
    if (priv.demux_factory == nullptr)
    {
        auto deps = broker_GetSharedDependencies("demux", module);
        const auto prefetch_deps = broker_GetSharedDependencies("stream_filter", "prefetch");
        const auto cache_deps = broker_GetSharedDependencies("stream_filter", "cache");

        for (const auto e : prefetch_deps)
            deps.emplace(e);

        for (const auto e : cache_deps)
            deps.emplace(e);

        try
        {
            priv.demux_factory = std::make_unique<DemuxFactory>(vlc_object_instance(demux),
                                                                "vlc-demux-service", deps,
								priv.demux_channel_to, std::move(priv.demux_channel_from),
								priv.demux_interrupt_to, std::move(priv.demux_interrupt_from));
        }
        catch (const std::runtime_error &)
        {
            return -1;
        }
    }

    auto& demux_factory = priv.demux_factory->proxy;
    ipc::PortId demux_factory_port = priv.demux_factory->port_id;

    // Step 4: Serialize partial demux and create remote object.
    vlc::RemoteAccess remote_access = { stream_proxy->remote_port(), stream_proxy->remote_id() };
    vlc::RemoteEsOut remote_esout = { priv.esout_portid, esout_id };

    rpc::ObjectId demux_object = 0;
    std::printf("[BROKER] Demux factory [id=%lu, port=%lu]\n", demux_factory->remote_id(), demux_factory_port);

    auto *interrupt_context = static_cast<InterruptContext *>(vlc_interrupt_getdata());
    assert(interrupt_context != nullptr);
    if (interrupt_context->demux_proxy == nullptr)
    {
        if (!demux_factory->interrupt_create(&interrupt_context->demux_id))
        {
            std::printf("[BROKER] Call to DemuxFactory::interrupt_create(...) failed\n");
            return -1;
        }

        const ipc::PortId demux_interrupt_port = priv.demux_factory->interrupt_port_id;
        interrupt_context->demux_proxy = priv.interrupt_channel->connect<vlc::InterruptibleProxy>(
            demux_interrupt_port, interrupt_context->demux_id);
    }

    if (!demux_factory->demux_create(remote_access, remote_esout, module, url, demux->b_preparsing,
                                     interrupt_context->demux_id, &demux_object))
    {
        std::printf("[BROKER] Call to DemuxFactory::demux_create(...) failed\n");
        return -1;
    }

    if(demux_object == 0)
    {
        std::printf("[BROKER] Creation of new demux failed\n");
        return -1;
    }

    std::printf("[BROKER] demux object id: %lu\n", demux_object);

    // Step 5: Proxify the passed demux object
    // TODO: Cleanup, this is a bit repetitive
    remote_stream_t rs = { remote_access.object_id, remote_access.port };
    remote_esout_t re = { remote_esout.object_id, remote_esout.port };
    remote_demux_t rd;
    rd.port = demux_factory_port;
    rd.stream = rs;
    rd.esout = re;
    rd.object_id = demux_object;

    vlc_rpc_ProxifyDemux(demux, &rd, *priv.broker_channel);

    return 0;
}

static void vlc_broker_ForwardInterrupt(void *opaque)
{
    std::lock_guard<std::mutex> lock(remote_stream_lock_);
    auto *ctxt = static_cast<InterruptContext *>(opaque);

    const bool kill = vlc_interrupt_killed(ctxt->context);

    if (ctxt->demux_proxy != nullptr)
        ctxt->demux_proxy->interrupt(kill);

    if (ctxt->access_proxy != nullptr)
        ctxt->access_proxy->interrupt(kill);
}

void vlc_broker_InterruptRegister(struct vlc_interrupt *interrupt)
{
    vlc_interrupt_set(interrupt);
    vlc_interrupt_register(vlc_broker_ForwardInterrupt, new InterruptContext{interrupt});
}

void vlc_broker_InterruptUnregister(struct vlc_interrupt *interrupt)
{
    vlc_interrupt_set(interrupt);
    auto *context = static_cast<InterruptContext *>(vlc_interrupt_getdata());
    delete context;
    vlc_interrupt_unregister();
}
