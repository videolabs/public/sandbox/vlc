#ifndef RPC_BROKER_HH
#define RPC_BROKER_HH

#ifdef __cplusplus
extern "C" {
#endif

int vlc_broker_Init();
int vlc_broker_Deinit();
int vlc_broker_CreateAccess(stream_t* s);
int vlc_broker_CreateDemux(demux_t* s, const char* module, const char* filepath);
void vlc_broker_EnableDialogs(bool enable);

void vlc_RemoteStream_Destroy(stream_t *);
void vlc_RemoteDemux_Destroy(stream_t *);

void vlc_broker_InterruptRegister(struct vlc_interrupt *);
void vlc_broker_InterruptUnregister(struct vlc_interrupt *);

#ifdef __cplusplus
}
#endif


#endif
