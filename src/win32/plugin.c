/*****************************************************************************
 * plugin.c : Low-level dynamic library handling
 *****************************************************************************
 * Copyright (C) 2001-2011 VLC authors and VideoLAN
 *
 * Authors: Sam Hocevar <sam@zoy.org>
 *          Ethan C. Baldridge <BaldridgeE@cadmus.com>
 *          Hans-Peter Jansen <hpj@urpla.net>
 *          Gildas Bazin <gbazin@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_charset.h>
#include "modules/modules.h"
#include <windows.h>
#include <wchar.h>

char *vlc_dlerror(void)
{
    wchar_t wmsg[256];
    int i = 0, i_error = GetLastError();

    FormatMessageW( FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                    NULL, i_error, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
                    wmsg, 256, NULL );

    /* Go to the end of the string */
    while( !wmemchr( L"\r\n\0", wmsg[i], 3 ) )
        i++;

    snwprintf( wmsg + i, 256 - i, L" (error %i)", i_error );
    return FromWide( wmsg );
}

void *vlc_dlopen(const char *psz_file, bool lazy)
{
    wchar_t *wfile = ToWide (psz_file);
    if (wfile == NULL)
        return NULL;

    HMODULE handle = NULL;
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP)
    DWORD mode;
    if (SetThreadErrorMode (SEM_FAILCRITICALERRORS, &mode) != 0)
    {
        handle = LoadLibraryExW(wfile, NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
        SetThreadErrorMode (mode, NULL);
    }
#elif WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP)
    handle = LoadPackagedLibrary( wfile, 0 );
#elif defined(WINAPI_FAMILY_GAMES) && (WINAPI_FAMILY == WINAPI_FAMILY_GAMES)
    // SEM_FAILCRITICALERRORS is not available on Xbox, error messages are not
    // possible. This is a sandboxed environment where the library dependencies
    // need to be managed manually per platform.
    handle = LoadLibraryExW(wfile, NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
#else
#error Unknown Windows Platform!
#endif
    free (wfile);

    (void) lazy;
    return handle;
}

int vlc_dlclose(void *handle)
{
    FreeLibrary( handle );
    return 0;
}

static inline const IMAGE_NT_HEADERS *get_nt_headers(const HMODULE module)
{
    const uint8_t *module_addr = (uint8_t *)module;
    const IMAGE_DOS_HEADER *dos_hdr = (IMAGE_DOS_HEADER *)module;

    return (IMAGE_NT_HEADERS *)(module_addr + dos_hdr->e_lfanew);
}

static inline const IMAGE_IMPORT_DESCRIPTOR *get_import_descriptor(const HMODULE module)
{
    const IMAGE_NT_HEADERS *nt_hdr = get_nt_headers(module);
    const DWORD_PTR image_base = (DWORD_PTR)module;

    const IMAGE_DATA_DIRECTORY *import_desc_dir =
        &(nt_hdr->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT]);
    if (unlikely(import_desc_dir->Size == 0))
        return NULL;

    return (IMAGE_IMPORT_DESCRIPTOR *)(image_base + import_desc_dir->VirtualAddress);
}

/**
 * Tell if a module path already exists in the dl dependencies list.
 *
 * \retval true Module path already exists.
 * \retval false Module path doesn't exist yet.
 */
static bool is_module_duplicate(const char *modname, const dldeps_t *modules)
{
    for (size_t i = 0; i < modules->size; ++i)
    {
        if (strncmp(modules->data[i], modname, MAX_PATH) == 0)
            return true;
    }
    return false;
}

/**
 * Parse the DLL dependencies of a given module.
 *
 * This function will parse the PE headers of the given module to output an unique list of all its
 * DLL dependencies file paths.
 *
 * \note Considering we only parse already loaded libraries, no bound check is done during parsing
 *       and the PE file is assumed to be sane. We assume that a corrupted DLL would've been caught
 *       by \ref LoadPackagedLibrary().
 *
 * \param[in] module Module handle.
 * \param[out] out Output dependency list.
 */
static int parse_module_dependencies(const HMODULE module, dldeps_t *out)
{
    const IMAGE_IMPORT_DESCRIPTOR *import_desc = get_import_descriptor(module);
    if (unlikely(import_desc == NULL))
        return VLC_SUCCESS;

    const DWORD_PTR image_base = (DWORD_PTR)module;

    /* Loop through all import descriptors, the end is an empty(zeroed) descriptor. */
    for (; import_desc->Name; ++import_desc)
    {
        const char *import_name = (char *)(image_base + import_desc->Name);
        HMODULE submodule = GetModuleHandleA(import_name);
        if (unlikely(submodule == NULL))
            continue;

        wchar_t submodule_path[MAX_PATH];
        if (!GetModuleFileName(submodule, submodule_path, ARRAY_SIZE(submodule_path)))
        {
            vlc_dldeps_Delete(out);
            return VLC_EGENERIC;
        }

        char *submodule_bpath = FromWide(submodule_path);
        if (unlikely(submodule_bpath == NULL))
            goto nomem;

        /* Skip already parsed dependencies. */
        if (is_module_duplicate(submodule_bpath, out))
        {
            free(submodule_bpath);
            continue;
        }

        if (!vlc_vector_push(out, submodule_bpath))
            goto nomem;

        /* Recursively parse the submodule. */
        const int result = parse_module_dependencies(submodule, out);
        if (result != VLC_SUCCESS)
            return result;
    }
    return VLC_SUCCESS;
nomem:
    vlc_dldeps_Delete(out);
    return VLC_ENOMEM;
}

int vlc_dldeps(void *handle, dldeps_t *out) { return parse_module_dependencies(handle, out); }

void *vlc_dlsym(void *handle, const char *psz_function)
{
    return (void *)GetProcAddress(handle, psz_function);
}
